;(function(context) {

	var debounce,tests;

	if(context) {
		debounce = context.debounce;
		tests = context.tests;
	} else {
		debounce = require('./debounce.js');
		tests = require('./tests.js');
	}

	var 
		hideDropdownTimeout,
		scrollDebounce = debounce(),
		resizeDebounce = debounce(),
		$window = $(window),
		$document = $(document),
		$html = $('html'),
		$body = $('body'),
		$ddContainerWrap = $('.nav-dd-section-wrap'),
		$ddContainer = $('.nav-dd-section'),
		$searchForm = $('.global-search-form'),
		$searchInput = $('input',$searchForm),
		startingNavOffset = 150,
		SHOW_SEARCH_CLASS = 'show-search',
		SHOW_CLASS = 'show-nav',
		SMALL_NAV = 'small-nav',
		SHOW_DROPDOWN_CLASS = 'show-dropdown',
		COLLAPSE_NAV_AT = 825,
		SMALL_DROPDOWNS_AT_HEIGHT = 600,
		SELECTED_LI_CLASS = 'selected';

	var methods = {
	
		isShortScreen: function() {
			return $window[0].innerHeight < SMALL_DROPDOWNS_AT_HEIGHT;
		},
	
		checkShowSmallNav: function() {
			if(!this.isNavCollapsed() && $window.scrollTop() > startingNavOffset) {
				$body.addClass(SMALL_NAV);
			} else {
				$body.removeClass(SMALL_NAV);
			}
		},
		
		isNavCollapsed: function() {
			return $window[0].innerWidth < COLLAPSE_NAV_AT;
		},

		onScroll: function() {
			this.checkShowSmallNav();
		},
		
		onResize: function() {
			this.checkShowSmallNav();
			this.isNavCollapsed() || this.showNav(false);
		},
	
		showNav: function(show) {
			$html[show ? 'addClass' : 'removeClass'](SHOW_CLASS);
		},

		toggleNav: function() {
			this.showNav(!this.isShowingNav());
		},

		isShowingNav: function() {
			return $html.hasClass(SHOW_CLASS);
		},
		
		showDropdown: function(el) {
			clearTimeout(hideDropdownTimeout);
			var height;
			
			el.find('div.lazybg').addClass('force');
			
			/* super simple dropdowns on screens that are too short */
			if(this.isShortScreen()) { return; }
			
			el
				.closest('li')
				.addClass(SELECTED_LI_CLASS)
				.siblings()
				.removeClass(SELECTED_LI_CLASS);
			
			$ddContainer.html(el.html())
			
			$ddContainer.find('.swiper-wrapper-to-be').each(function(i,el) {
				$(this).addClass('swiper-wrapper')
				$document.trigger('updateTemplate.swiper');
			});
			
			height = $ddContainer.children('div.dd').outerHeight();
			
			$ddContainer.css({height:height});
			$body
				.css({paddingTop:height})
				.addClass(SHOW_DROPDOWN_CLASS);
			
			setTimeout(function() { $document.trigger('updateTemplate'); },400);
		},
		
		hideDropdown: function() {
			hideDropdownTimeout = setTimeout(function() {
				$ddContainer.css({height:0});
				$body
					.css({paddingTop:0})
					.removeClass(SHOW_DROPDOWN_CLASS);
				
				$('nav>ul>li').removeClass(SELECTED_LI_CLASS);
			},200);
		},
		
		showSearch: function(show) {
			var $input = $('input',$searchForm);
			$html[show ? 'addClass' : 'removeClass'](SHOW_SEARCH_CLASS);
			setTimeout(function() {
				if(show) {
					$input.focus(); 
				} else {
					$input.blur();		
				}
			}, 400);
		},
		
		toggleSearch: function() {
			this.showSearch(!this.isShowingSearch());
		},
		
		isShowingSearch: function() {
			return $html.hasClass(SHOW_SEARCH_CLASS);
		}

	};
	
	//listeners
	$document
		.on('click','.toggle-nav',function(e) {
			methods.toggleNav();
			return false;
		})
		.on('keydown',function(e) {
			if(e.result !== false && e.keyCode === 27) {
				if(methods.isShowingSearch()) {
					methods.showSearch(false);
					return false;
				}
				
				if(methods.isShowingNav()) {
					methods.showNav(false);
					return false
				}
			}
		})
		.on('mouseenter','nav>ul>li>a',function() {
			var 
				el = $(this),
				li = el.parent(),
				dd = el.siblings('div.dd-wrap');
			
			!li.hasClass(SELECTED_LI_CLASS) && dd.length && methods.showDropdown(dd);
			
		})
		.on('mouseleave','nav>ul>li>a',function(e) {
			if(e.relatedTarget !== $ddContainerWrap[0]) {
				methods.hideDropdown();
			}
		})
		.on('mouseleave','div.nav-dd-section-wrap',function(e) {
			if(!$(e.relatedTarget).closest('li.'+SELECTED_LI_CLASS).length) {
				methods.hideDropdown();
			}
		})
		.on('click','.toggle-search',function() {
			methods.toggleSearch();
		})
		.on('keypress',function(e) {
			if(e.keyCode === 63 || (e.shiftKey && e.keyCode === 191)) {
				methods.showSearch(true);
			}
		})
		.on('click','nav div',function(e) {
			if(e.currentTarget.nodeName.toLowerCase() === 'div') {
				$(e.currentTarget).parent().toggleClass('expanded');
			}
		})

	$window
		.on('scroll',function() {
			scrollDebounce.requestProcess(methods.onScroll,methods);
		})
		.on('resize',function() {
			resizeDebounce.requestProcess(methods.onResize,methods);
		});
		
		if(tests.ios()) {
			
			//onScroll must be fired continuously
			(function iOSOnScroll() {
				methods.onScroll();
				requestAnimationFrame(function() { iOSOnScroll(); });
			}());
		
		} else {
			//fire immediately
			methods.onScroll();
			methods.onResize();
		}
	
}(typeof ns !== 'undefined' ? window[ns] : undefined));
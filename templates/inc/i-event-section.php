			<div class="bordered pad-40">
				
				<div class="section-header">
					<h2 class="title">Event Schedule</h2>
					<span class="subtitle">Amazing instructors & workshops. Check them out!</span>
				</div><!-- .section-header -->

				<div class="grid date-grid centered">
				
					<div class="col">
						<div class="item">
							<a class="date-block" href="#">
								<span class="dow">Wednesday</span>
								<span class="date">14</span>
								<span class="month">October</span>
							</a><!-- .date-block -->
						</div><!-- .item -->
					</div><!-- .col -->
					
					<div class="col">
						<div class="item">
							<a class="date-block" href="#">
								<span class="dow">Thursday</span>
								<span class="date">15</span>
								<span class="month">October</span>
							</a><!-- .date-block -->
						</div><!-- .item -->
					</div><!-- .col -->
					
					<div class="col">
						<div class="item">
							<a class="date-block" href="#">
								<span class="dow">Friday</span>
								<span class="date">16</span>
								<span class="month">October</span>
							</a><!-- .date-block -->
						</div><!-- .item -->
					</div><!-- .col -->
					
					<div class="col">
						<div class="item">
							<a class="date-block" href="#">
								<span class="dow">Saturday</span>
								<span class="date">17</span>
								<span class="month">October</span>
							</a><!-- .date-block -->
						</div><!-- .item -->
					</div><!-- .col -->
					
					<div class="col">
						<div class="item">
							<a class="date-block" href="#">
								<span class="dow">Sunday</span>
								<span class="date">18</span>
								<span class="month">October</span>
							</a><!-- .date-block -->
						</div><!-- .item -->
					</div><!-- .col -->
					
				</div><!-- .grid -->
				
				<br />
				
				<div class="center">
					<a href="#" class="button blue-o big">View Full Schedule</a>
				</div>
		
				<div class="schedule swiper-wrapper dark-bg i-hidden">
				
					<div class="swiper">
						<div class="swipe-item">
						
							<div class="schedule-timeline-grid-wrap">
								<div class="schedule-timeline grid">
								
									<div class="col col-4">
										<div class="item">
											<div class="schedule-timeline-item">
											
												<div class="schedule-timeline-block">
													<i class="fa fa-pencil"></i>
													<span class="schedule-timeline-title">11:00AM</span>
													<span class="schedule-timeline-subtitle">Registration</span>
												</div><!-- .schedule-timeline-block -->
												<div class="schedule-timeline-block">
													<i class="fa fa-building-o"></i>
													<span class="schedule-timeline-title">Holiday Inn</span>
													<span class="schedule-timeline-subtitle">Gros Morne, NL</span>
												</div><!-- .schedule-timeline-block -->
											
											</div><!-- .schedule-timeline-item -->
											
										</div><!-- .item -->
									</div><!-- .col -->
									
									<div class="col col-4">
										<div class="item">
											<div class="schedule-timeline-item">
												<div class="schedule-timeline-block">
													<i class="fa fa-lightbulb-o"></i>
													<span class="schedule-timeline-title">12:00AM</span>
													<span class="schedule-timeline-subtitle">Registration</span>
												</div><!-- .schedule-timeline-block -->
												<div class="schedule-timeline-block">
													<i class="fa fa-building-o"></i>
													<span class="schedule-timeline-title">Holiday Inn</span>
													<span class="schedule-timeline-subtitle">Gros Morne, NL</span>
												</div><!-- .schedule-timeline-block -->
											</div><!-- .schedule-timeline-item -->
										</div><!-- .item -->
									</div><!-- .col -->

									<div class="col col-4">
										<div class="item">
											<div class="schedule-timeline-item">
												<div class="schedule-timeline-block">
													<i class="fa fa-pencil"></i>
													<span class="schedule-timeline-title">11:00AM</span>
													<span class="schedule-timeline-subtitle">Registration</span>
												</div><!-- .schedule-timeline-block -->
												<div class="schedule-timeline-block">
													<i class="fa fa-building-o"></i>
													<span class="schedule-timeline-title">Holiday Inn</span>
													<span class="schedule-timeline-subtitle">Gros Morne, NL</span>
												</div><!-- .schedule-timeline-block -->
											</div><!-- .schedule-timeline-item -->
										</div><!-- .item -->
									</div><!-- .col -->
									
									<div class="col col-4">
										<div class="item">
											<div class="schedule-timeline-item">
												<div class="schedule-timeline-block">
													<i class="fa fa-lightbulb-o"></i>
													<span class="schedule-timeline-title">12:00AM</span>
													<span class="schedule-timeline-subtitle">Registration</span>
												</div><!-- .schedule-timeline-block -->
												<div class="schedule-timeline-block">
													<i class="fa fa-building-o"></i>
													<span class="schedule-timeline-title">Holiday Inn</span>
													<span class="schedule-timeline-subtitle">Gros Morne, NL</span>
												</div><!-- .schedule-timeline-block -->
											</div><!-- .schedule-timeline-item -->
										</div><!-- .item -->
									</div><!-- .col -->
									
								</div><!-- .schedule-timeline -->
							
							</div><!-- .schedule-timeline-grid-wrap -->
							
							<a href="#" class="button">View Full Schedule</a>
							
						</div><!-- .swipe-item -->
					</div><!-- .swiper -->
					
					
					
				</div><!-- .schedule -->
		
			</div><!-- .bordered -->
